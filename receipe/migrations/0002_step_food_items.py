# Generated by Django 4.1 on 2022-08-25 17:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipe", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="step",
            name="food_items",
            field=models.ManyToManyField(
                blank=True, null="True", to="receipe.fooditem"
            ),
            preserve_default="True",
        ),
    ]
