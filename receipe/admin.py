from django.contrib import admin
from receipe.models import Receipe, Measure, FoodItem, Ingredient, Step

# Register your models here.

admin.site.register(Receipe)
admin.site.register(Measure)
admin.site.register(FoodItem)
admin.site.register(Ingredient)
admin.site.register(Step)
