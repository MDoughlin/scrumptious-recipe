from django.db import models


class Tags(models.Model):
    name = models.CharField(max_length=20)
    recipes = models.ManyToManyField(
        "receipe.Receipe",
        related_name="tags"
        )

    def __str__(self):
        return self.name
